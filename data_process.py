import numpy as np

def moving_average(a, n=3) :
    m_avg = np.convolve(a, np.ones((n,))/n, mode='valid')
    new_arr = np.zeros_like(a)
    diff = (new_arr.size - m_avg.size)/2
    diff = int(np.floor(diff))
    new_arr[diff:new_arr.size-diff] = m_avg
    return new_arr

def get_cumulative_data(day_data):
    cum_data = np.zeros_like(day_data)
    flipped_data = np.flipud(day_data)
    cum_data[:,0] = np.cumsum(flipped_data[:,0])
    cum_data[:,1] = np.cumsum(flipped_data[:,1])
    cum_data[:,2] = day_data[:,2]
    cum_data = np.flipud(cum_data)
    return cum_data

def normalise_by_pop(case_data):
    case_per_thou = np.divide(case_data[:, 0], case_data[:, 2])*1000
    deaths_per_thou = np.divide(case_data[:, 1], case_data[:, 2])*1000
    data = np.c_[case_data, case_per_thou, deaths_per_thou]
    return data

def get_rate(case_data):
    death_rate = np.divide(case_data[:,1], case_data[:,0])
    data = np.c_[case_data, death_rate]
    return data

def get_deltas(case_data, col):
    diff_array = np.zeros_like(case_data[:, col])
    diff_array[0:-1] = case_data[0:-1, col] - case_data[1:, col]
    smoothed = moving_average(diff_array,n=11)
    return smoothed

def cut_out_datecodes(country_d):
    dates = []
    for i in country_d['dates']:
        dates.append(i[0])
    country_d['datecodes'] = dates
    return country_d

def get_case_data(master_dict, country):
    return master_dict[country]["case_data"]
def get_cum_data(master_dict, country):
    return master_dict[country]["cum_case_data"]
