import numpy as np
import pickle
import os
from data_process import *

ECDC_PKL_FILE = "ecdc_data.pkl"
ECDC_CSV_FILE = "./ecdc_CV_data.csv"

def load_csv(filename):
    case_data = []
    date_data = []
    master_data = {}
    country_name = 'Afghanistan'
    country_code = 'AF'
    with open(filename, 'r') as infile:
        next(infile)
        for line in infile:
            line = line.rstrip('\r\n')
            line = line.split(',')
            if len(line)==10:
                for i in xrange(len(line)):
                    if line[i] == '' and i not in [0,6,7,8]:
                        line[i] = 0.0
                    elif line[i] == '':
                        line[i] = 'NULL'
                    if line[7] != country_code:
                        master_data[country_code] = {
                            'country_name' : country_name,
                            'case_data' : case_data,
                            'dates' : date_data
                        }
                        case_data = []
                        date_data = []
                        country_code = line[7]
                        country_name = line[6]
                case_data.append([line[4],line[5],line[9]])
                date_data.append(line[0])
    infile.close()
    for e in master_data:
        data = master_data[e]['case_data']
        for i in xrange(len(data)):
            for j in xrange(len(data[i])):
                try:
                    x=float(data[i][j])
                except ValueError:
                    data[i][j] = 0.0

        master_data[e]['case_data'] = np.array(data).astype(float)

    return master_data

def load_ecdc_data(filename):
    master_data = load_csv(filename)
    #Case data has 3 cols: day_case, day_death, pop
    for k in master_data:
        country_dict = master_data[k]
        country_dict['cum_case_data'] = get_cumulative_data(country_dict['case_data'])
        #This adds 2 columns: cases_per thou, deaths_per_thou
        country_dict['case_data'] = normalise_by_pop(country_dict['case_data'])
        country_dict['cum_case_data'] = normalise_by_pop(country_dict['cum_case_data'])

        #This add one column the death_rate
        country_dict['case_data'] = get_rate(country_dict['case_data'])
        country_dict['cum_case_data'] = get_rate(country_dict['cum_case_data'])
        #Array is /cases / deaths / cases_per_1000 / deaths_per_1000 / death_rate

        country_dict['delta_case'] = get_deltas(country_dict['case_data'], 0)
        country_dict['delta_death'] = get_deltas(country_dict['case_data'], 1)
        country_dict = cut_out_datecodes(country_dict)
    pickle.dump(master_data, open( "ecdc_data.pkl", "wb" ) )
    return master_data

def get_ecdc_data():
    if os.path.exists(ECDC_PKL_FILE):
        ecdc_data = pickle.load( open(ECDC_PKL_FILE, 'rb'))
    else:
        ecdc_data=load_ecdc_data(ECDC_CSV_FILE)
    return ecdc_data
