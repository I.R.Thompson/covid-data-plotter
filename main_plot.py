import datetime
import pickle
import matplotlib.dates as mdates
import matplotlib.pyplot as plt

from data_process import *
from load_data import *

def set_dates_x(axis):
    formatter = mdates.DateFormatter("%d/%m")
    axis.xaxis.set_major_formatter(formatter)
    plt.setp( axis.get_xmajorticklabels(), rotation=90 )
    return axis

def make_a_plot(master_data, dict_entry, col, titles, plot_list, ax, drop_last=False):
    legend_list = []
    ax = set_dates_x(ax)
    for k in plot_list:
        country_dict = master_data[k]
        legend_list.append(country_dict["country_name"])
        x_values = [datetime.datetime.strptime(d,"%d/%m/%Y").date() for d in country_dict['dates']]
        if country_dict[dict_entry].ndim>1:
            y_values = country_dict[dict_entry][:,col]
        else:
            y_values = country_dict[dict_entry][:]
        if drop_last:
            x_values = x_values[:-1]
            y_values = y_values[:-1]
        ax.set_title(titles[col])

        ax.plot(x_values, y_values)
    ax.legend(legend_list)

ecdc_data = get_ecdc_data()
fields = ["Day cases", "Day deaths", "Population (2018)",
          "Day cases per thou", "Day deaths per thou",
          "Death rate"]
cum_fields = ["Cumulative "+s for s in fields]
plot_country_list = ['UK', 'NL', 'DE', 'IT', 'KR', 'BE']
legend_list = []

for idx in [3,4,5]:
    f, (ax1, ax2) = plt.subplots(1, 2, figsize=(10,6), sharey=False)
    make_a_plot(ecdc_data, 'case_data', idx, fields, plot_country_list, ax1)
    make_a_plot(ecdc_data, 'cum_case_data', idx, cum_fields, plot_country_list, ax2)
    plt.show()

f, (ax1, ax2) = plt.subplots(1, 2, figsize=(10,6), sharey=False)
make_a_plot(ecdc_data, 'delta_case', 0, ['Delta cases (moving average=3)'], plot_country_list, ax1, drop_last=True)
make_a_plot(ecdc_data, 'delta_death', 0, ['Delta deaths (moving average=3)'], plot_country_list, ax2, drop_last=True)
plt.show()
