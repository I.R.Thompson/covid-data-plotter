# Covid data plotter

Originally made to plot data released by the ECDC in CSV format.
Made quickly to calculate and compare some like numbers from the same dataset.
Presented without comment, it just makes graphs, you do what you want with them.

Data in CSV format available form ECDC at https://www.ecdc.europa.eu/en/publications-data/download-todays-data-geographic-distribution-covid-19-cases-worldwide

To make plots download the CSV data, rename the file to "ecdc_CV_data.csv" and run main_plot.py 
